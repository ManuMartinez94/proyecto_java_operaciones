/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package llistaOperacions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author manu_
 */
public class MetodesFitxers {

    /*Guarda al fitxer les dades amb el format indicat*/
    /*Si ocurreix un error, llança una excepció*/
    public static void guardar(ArrayList<Operacions> llista, File f) throws IOException {
        String fila = "";
        FileWriter fw = new FileWriter(f, true);
        for (Operacions o : llista) {
            fila += o.getTipus() + "|";
            fila += o.getDescripcio() + "|";
            fila += String.valueOf(o.getImport());

            fw.write(fila + "\n");
            fw.write("-----\n");
            fila = "";
        }
        fw.close();
    }

    /*Torna una llista amb les línies llegides del fitxer*/
    /*Si ocurreix un error, llança una excepció*/
    public static ArrayList<String> carregar(File f) throws FileNotFoundException, IOException {
        ArrayList<String> llistaFiles = new ArrayList<>();
        FileReader fr = new FileReader(f);
        Scanner lector = new Scanner(f);
        while (lector.hasNext()) {
            llistaFiles.add(lector.nextLine());
        }
        fr.close();
        return llistaFiles;

    }
}
