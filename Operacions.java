/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package llistaOperacions;

/**
 *
 * @author manu_
 */
public class Operacions {
    private String tipus;
    private String descripcio;
    private int Import;
    
    /*Dins del constructor, canvia a negatiu l'import si el tipus es Venda*/
    public Operacions(String tipus, String descripcio, int Import) {
        this.tipus = tipus;
        if(this.tipus.equals("Venda")&&Import>0)Import = -Import;
        this.descripcio = descripcio;
        this.Import = Import;
    }

    public String getTipus() {
        return tipus;
    }

    public void setTipus(String tipus) {
        this.tipus = tipus;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public int getImport() {
        return Import;
    }

    public void setImport(int Import) {
        this.Import = Import;
    }


}
